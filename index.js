const kafka = require('kafka-node');
const Producer = kafka.Producer;
const client = new kafka.KafkaClient({ kafkaHost: 'master:9092,slave:9092' });
const producer = new Producer(client);
const payloads = [
    { topic: 'esgi', messages: 'Hello world !' },
];
const express = require('express');
const app = express();
const port = 8081;

producer.on('ready', () => {
    app.get('/', (req, res) => {
        producer.send(payloads, function (err, data) {
            res.send(arguments);
        });
    });

    app.get('/health', (req, res) => res.send('Hello World'));

    app.listen(port, () => console.log(`Example app listening on port ${port}!`));
});
