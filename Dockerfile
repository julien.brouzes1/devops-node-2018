FROM node:latest

WORKDIR /workspace

COPY . /workspace


EXPOSE 8081

RUN npm install

CMD ["npm", "start"]